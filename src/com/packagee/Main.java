package com.packagee;

public class Main {

    public static void main(String[] args) {

        Quiz quiz = new QuizImpl();
        int minRange=Quiz.MIN_VALUE;
        int maxRange=Quiz.MAX_VALUE;
        int digit = (Quiz.MAX_VALUE+Quiz.MIN_VALUE)/2; // zainicjuj zmienna

        for(int counter = 1; ;counter++) {

            try {

                quiz.isCorrectValue(digit);
                System.out.println("Trafiona proba!!! Szukana liczba to: "
                        + digit + " Ilosc prob: " + counter);
                break;

            }
            catch (Quiz.ParamTooLarge paramTooLarge) {
                System.out.println("Argument za duzy!!!");
                maxRange=digit-1;
                digit= (minRange+maxRange)/2;
            }
            catch (Quiz.ParamTooSmall paramTooSmall) {
                System.out.println("Argument za maly!!!");
                minRange=digit+1;
                digit= (minRange+maxRange)/2;
            }
        }
    }
}
